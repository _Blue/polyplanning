from typing import List, Dict
from .range import Range

class StaticScore:
    pass


class WeekDayScore(StaticScore):
    def __init__(self, days_scores: List[float]):
        assert len(days_scores) == 7 and [0 <= e <= 1 for e in days_scores]
        self.days_scores = days_scores

    def compute_score(self, range: Range) -> float:
        return self.days_scores[range.start.weekday()]


class RelativeScore:
    pass

class DistanceScore(RelativeScore):
    def __init__(self, relative_scores: Dict[int, float]):
        assert all(-1 <= e <= 1 for e in relative_scores.values())
        self.relative_scores = relative_scores

    def compute_score(self, left: Range, right: Range) -> float:
        days = abs((left.start - right.start).days)

        if days in self.relative_scores:
            return self.relative_scores[days]
        else:
            return 0
