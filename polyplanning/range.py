from datetime import datetime



class Range:
    def __init__(self, start: datetime, end: datetime):
        assert start <= end

        self.start = start
        self.end = end

    def __repr__(self):
        return f'[{self.start} - {self.end}]'

    def intersection(self, range):
        start = max(self.start, range.start)
        end = min(self.end, range.end)

        if start > end:
            return None

        return Range(start, end)

    def intersects(self, range) -> bool:
        return self.intersection(range) is not None

    def contains(self, ts: datetime):
        return self.start >= ts >= self.end
