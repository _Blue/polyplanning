import logging
import itertools
from typing import List, Dict
from .context import Context, UserContext
from .event import Event
from .range import Range
from icalendar.cal import Component
from icalendar.cal import Event as CalDavEvent


def find_matching_user_events(event: Event, user_events: Dict[str, Event]):
    result = {}

    for user,events in user_events.items():
        result[user] = [e for e in sub_events if e.intersects(event)]

    return result

class Solution:
    def __init__(self, context: Context, existing_events:Dict[str, Event]={}):
        self.new_events = []
        self.existing_events = self.generate_existing_events(existing_events)
        self.context = context


    def create_existing_event(self, range: Range) -> Event:
        event = CalDavEvent()
        event.add('dtstart', range.start)
        event.add('dtend', range.end)
        event['summary'] = '[Existing event]'

        return Event(event)


    def generate_existing_events(self, user_events: Dict[str, Event]):
        existing_events = []

        all_events = list(itertools.chain.from_iterable(e for e in user_events.values()))

        # Note: Iterate like this to make sure that events that are incoherent between users
        # are not forgotten
        while all_events:
            existing_events.append(self.create_existing_event(all_events[0]))
            all_events = [e for e in all_events if not e.intersects(all_events[0])]

        return existing_events

    @property
    def events(self):
        return sorted(self.new_events + self.existing_events, key=lambda e: e.start)

    def add_event(self, candidate: Event):
        assert candidate not in self.events
        self.events += [candidate]

    def with_event(self, event: Event):
        new_solution = Solution(self.context)
        new_solution.new_events = [e for e in self.new_events]
        new_solution.new_events.append(event)
        new_solution.existing_events = self.existing_events

        return new_solution

    def __repr__(self):
        total = 0

        out = ''
        for e in self.events:
            score = self.event_score(e)
            out += f'Score: scatic={score[0]}, dynamic={score[1]} - {e}\n'
            total += sum(score)

        return f'{{Total score: {total}\n{out}}}'

    def event_score(self, event: Range):
        user_score = sum(e.event_score(event.range) for e in self.context.users.values()) / len(self.context.users)
        relative_score = sum(self.context.dynamic_score(event.range, e.range) for e in self.events)

        return user_score, relative_score

    def score(self):
        return sum(sum(self.event_score(e)) for e in self.events)


class MissingEventWarning:
    def __init__(self, event: Event, organiser: str, missing_user: str):
        self.event = event
        self.organiser = organiser
        self.missing_user = missing_user

    def __repr__(self):
        return f"Event '{self.event}' scheduled by '{self.organiser}', but not found in {self.missing_user}'s calendar"

class Result:
    def __init__(self, candidates: List[Event], solutions: List[Solution], existing_events: List[Event], warnings: List[str]):
        self.candidates = candidates
        self.solutions = solutions
        self.existing_events = existing_events
        self.warnings = warnings

class Solver:
    def find_existing_events(self, user: UserContext, context: Context) -> List[Event]:
        return [e for e in user.calendar.find_events_in_range(context.planning_range) if user.matches_event_filter(e)]

    def validate_existing_event_coherence(self, user_events: Dict[str, List[Event]]) -> List[str]:
        warnings = []

        for user, events in user_events.items():
            for event in events:
                for sub_user, sub_events in user_events.items():
                    if sub_user == user:
                        continue

                    if not any(e for e in sub_events if e.intersects(event)):
                        warnings.append(MissingEventWarning(event, user, sub_user))
                        logging.warn(str(warnings[-1]))

        return warnings

    def solve(self, context: Context) -> Result:
        existing_events = {name:self.find_existing_events(user, context) for name,user in context.users.items()}
        logging.debug(f'Found {sum(len(e) for e in existing_events.values())} existing events')
        warnings = self.validate_existing_event_coherence(existing_events)

        candidates = self.generate_candidates(context)
        logging.debug(f'Generated {len(candidates)} candidates')

        if len(candidates) > context.max_candidates:
            warnings.append('Too many candidates, capping at {context.max_candidates}')
            logging.warn(warnings[-1])
            candidates = candidates[:context.max_candidates]

        null_solution = Solution(context, existing_events)
        solutions = self.generate_from(null_solution, [e for e in candidates], 0, context)
        logging.debug(f'Found {len(solutions)} solutions')

        return Result(candidates,
                      list(reversed(sorted([null_solution] + solutions, key=lambda e: e.score()))),
                      existing_events,
                      warnings)
