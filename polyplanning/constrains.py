from typing import List
from datetime import timedelta, date
from .range import Range
from .calendar import Calendar

class Constrain:
    pass

class EventMarginConstrain(Constrain):
    def __init__(self, before: timedelta, after: timedelta):
        self.before = before
        self.after = after

    def evaluate(self, range: Range, calendar: Calendar) -> bool:
        return not any(calendar.find_events_in_range(Range(range.start - self.before, range.end + self.after)))


class ExcludedDaysConstraint(Constrain):
    def __init__(self, excluded: List[date]):
        self.excluded = excluded

    def evaluate(self, range: Range, calendar: Calendar) -> bool:
        return not(any(range.start.year == e.year and range.start.month == e.month and range.start.day == e.day for e in self.excluded))
