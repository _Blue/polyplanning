import icalendar
import requests
from typing import List
from icalendar.cal import Component
from .range import Range
from .event import Event, RecurringEvent


class Calendar:
    def __init__(self, events: List[Component]):
        self.events = events

    def find_events_in_range(self, range: Range):
        return [e for e in self.events if e.intersects(range)]

    @staticmethod
    def filter_events(events: List[Component]) -> List[Event]:

        def make_event(event: Component):
            if 'RRULE' in event:
                return RecurringEvent(event)
            else:
                return Event(event)

        return [make_event(e) for e in events if e.has_key('summary') and e.name != 'VTODO']

    @staticmethod
    def from_url(url: str, auth=None):
        session = requests.Session()
        session.auth = auth

        response = session.request(method='GET', url=url)
        response.raise_for_status()

        return Calendar(Calendar.filter_events(icalendar.Calendar.from_ical(response.text).subcomponents))

    @staticmethod
    def from_file(path: str):
        with open(path) as fd:
            return Calendar(Calendar.filter_events(icalendar.Calendar.from_ical(fd.read()).subcomponents))
