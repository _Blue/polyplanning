import requests
from icalendar.cal import Component
import icalendar
import icalendar.cal
from datetime import datetime, timedelta, date
from copy import copy
from .range import Range
from tzlocal import get_localzone
import pytz
from dateutil.rrule import rrulestr

def rationalize_time(ts) -> datetime:
    if not isinstance(ts, datetime): # In case of date, convert to datetime (set 00:00)
        assert isinstance(ts, date)
        return get_localzone().localize(datetime.combine(ts, datetime.min.time()))
    else:
        return ts.astimezone(get_localzone())

class Event:
    def __init__(self, event: Component):
        assert 'RRULE' not in event
        self.event = event

    def __repr__(self) -> str:
        return f'{self.event["summary"]} {self.range}'

    def intersects(self, range: Range) -> bool:
        return self.range.intersects(range)

    @property
    def start(self) -> datetime:
        if not 'dtstart' in self.event:
            return None

        return rationalize_time(self.event['dtstart'].dt)

    @property
    def end(self) -> datetime:
        if not 'dtend' in self.event:
            return None

        return rationalize_time(self.event['dtend'].dt)

    @property
    def range(self) -> Range:
        return Range(self.start, self.end)

    @property
    def duration(self) -> timedelta:
        assert self.start and self.end

        return self.end - self.start

    def move_start(self, new_start: datetime):
        new_event = copy(self.event)
        new_event['dtstart'] = icalendar.vDatetime(new_start)
        new_event['dtend'] = icalendar.vDatetime(new_start + self.duration)

        return Event(new_event)


class RecurringEvent(Event):
    def __init__(self, event: Component):
        assert 'RRULE' in event
        self.event = event


    def next_occurence(self, after: datetime) -> datetime:
        # note: rrulestr doesn't support localized datetimes
        # Because 'until' might be there, and potentially timezone un-aware, we have to deal with this

        until = self.event['rrule'].get('until')
        if until and isinstance(until, list):
            until = until[0]

        if until and (not hasattr(until, 'tzinfo') or until.tzinfo is None): # If until is there, and unaware of timezones
            rule = rrulestr(self.event['rrule'].to_ical().decode(), dtstart=self.start.replace(tzinfo=None))
            result = rule.after(self.start.replace(tzinfo=None))
            return result and result.replace(tzinfo=self.start.tzinfo)
        else:
            rule = rrulestr(self.event['rrule'].to_ical().decode(), dtstart=self.start)
            return rule.after(after.astimezone(pytz.UTC))

    def intersects(self, range: Range) -> bool:
        next = self.next_occurence(range.start - self.duration)
        return next and range.intersects(Range(next, next + self.duration))
