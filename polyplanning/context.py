from calendar import Calendar
from .constrains import Constrain
from .event import Event
from .range import Range
from .score import StaticScore, RelativeScore
from typing import List, Dict

class UserContext:
    def __init__(self, calendar: Calendar, constrains: List[Constrain], score_rules: List[StaticScore], event_filters: list = []):
        self.calendar = calendar
        self.constrains = constrains
        self.score_rules = score_rules
        self.event_filters = event_filters

    def event_score(self, event: Range) -> float:
        return sum(e.compute_score(event) for e in self.score_rules) / len(self.score_rules)

    def matches_event_filter(self, event: Event) -> bool:
        return any(e(event) for e in self.event_filters)

class Context:
    def __init__(self, users: Dict[str, UserContext], template: Event, planning_range: Range, score_rules: List[RelativeScore]):
        self.users = users
        self.template = template
        self.planning_range = planning_range
        self.score_rules = score_rules
        self.max_candidates = 20

    def dynamic_score(self, left: Range, right: Range) -> float:
        return sum(e.compute_score(left, right) for e in self.score_rules) / len(self.score_rules)

    def statisfies_constrains(self, range: Range) -> bool:
        return all(all(e.evaluate(range, user.calendar) for e in user.constrains) for user in self.users.values())
