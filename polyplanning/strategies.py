import logging
from datetime import datetime, timedelta
from typing import List
from .context import Context
from .range import Range
from .event import Event
from .solver import Solution, Solver

class WeeklyStrategy(Solver):
    def __init__(self, max_per_week: int):
        super()

        self.max_per_week = max_per_week

    def make_candidate(self, day: datetime, template: Event) -> Event:
        new_start = day.replace(hour=template.start.hour,
                                minute=template.start.minute,
                                second=template.start.second,
                                microsecond=template.start.microsecond)
        return template.move_start(new_start)

    def is_over_weekly_max_with(self, solution: Solution, event: Event) -> bool:
        solution_events = solution.events + [event]
        def count_in_range(range: Range) -> int:
            return sum(1 for e in solution_events if e.range.intersects(range))

        if not solution_events:
            return 0

        day = max(solution_events[0].range.start, event.range.start - timedelta(days=7))
        end = min(event.range.end + timedelta(days=7), solution_events[-1].range.start)
        while day <= end:
            if count_in_range(Range(day, day + timedelta(days=7))) > self.max_per_week:
                return True

            day = day + timedelta(days=1)

        return False

    def generate_candidates(self, context: Context) -> List[Event]:
        # First create all possible candidates
        candidates = []

        day = context.planning_range.start
        while day <= context.planning_range.end:
            candidates.append(self.make_candidate(day, context.template))
            day += timedelta(days=1)

        # Then keep only the ones that match the constrains
        return [e for e in candidates if context.statisfies_constrains(e.range) and e.start <= context.planning_range.end]

    def generate_from(self, solution: Solution, candidates: List[Event], current_score: float, context: Context) -> List[Solution]:
        new_solutions = []
        while candidates:
            current_candidate = candidates[0]

            candidates.pop(0)
            if not self.is_over_weekly_max_with(solution, current_candidate):
                new_solution = solution.with_event(current_candidate)
                new_score = new_solution.score()

                # Note: This makes the assumption that dynamic scores are always< 0 
                if new_score > current_score:
                    new_solutions.append(new_solution)
                    new_solutions += self.generate_from(new_solution, list(candidates), new_score, context)

        return new_solutions

