from setuptools import setup, find_packages

setup(
    name="polyplanning",
    version="1",
    packages=find_packages(),
    install_requires=[
        'icalendar>=4.0.1',
        'pytz>=2019.3',
        'python-dateutil>=2.8.1'
        ]
    )
